import { NgModule } from '@angular/core';
import { CommonFormComponent } from './common-form/common-form.component';


@NgModule({
  declarations: [CommonFormComponent],
  imports: [
  ],
  exports: [CommonFormComponent]
})
export class NgCommonFormModule { }
