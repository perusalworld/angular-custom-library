/*
 * Public API Surface of ng-common-form
 */

export * from './lib/ng-common-form.module';
export * from './lib/common-form/common-form.component';
