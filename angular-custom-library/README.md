# AngularCustomLibrary

## Build Library

Run `npm run package` to build the Library project. The build artifacts will be stored in the `dist/` directory.

## Install Library
 `npm install dist/ng-common-form/ng-common-form-0.0.1.tgz`.
 
## Run Project
`ng serve`
